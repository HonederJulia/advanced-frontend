$(document).ready(() => {
    let _containerHeight = 10000;
    let _width, _height, _scrollHeight;
    let _movingElements = [];
    let _scrollPercent = 0;
    let pre = prefix();
    let _jsPrefix = pre.lowercase;

    if (_jsPrefix == 'moz')
        _jsPrefix = 'Moz';

    let _cssPrefix = pre.css;

    let _positions = [

        // image_01
        {
            name: 'image_01_00',
            start: {
                percent: 0, x: 1.1, y: 0.05,
                rotate: 0
            },
            end: {
                percent: 1, x: 0.069, y: 0.9062,
                rotate: 0
            }
        },
        {
            name: 'image_01_10',
            start: {
                percent: 0, x: -0.9, y: 0,
                rotate: 0
            },
            end: {
                percent: 1, x: 0.150, y: 0.9062,
                rotate: 0
            }
        },
        {
            name: 'image_01_20',
            start: {
                percent: 0, x: 0, y: -0.05,
                rotate: 0
            },
            end: {
                percent: 1, x: 0.232, y: 0.9062,
                rotate: 0
            }
        },
        {
            name: 'image_01_01',
            start: {
                percent: 0, x: 1.1, y: 0,
                rotate: 0
            },
            end: {
                percent: 1, x: 0.069, y: 0.9219,
                rotate: 0
            }
        },
        {
            name: 'image_01_11',
            start: {
                percent: 0, x: 0, y: 0.11,
                rotate: 0
            },
            end: {
                percent: 1, x: 0.150, y: 0.9219,
                rotate: 0
            }
        },
        {
            name: 'image_01_21',
            start: {
                percent: 0, x: 1.1, y: 0,
                rotate: 0
            },
            end: {
                percent: 1, x: 0.232, y: 0.9219,
                rotate: 0
            }
        },
        {
            name: 'image_01_02',
            start: {
                percent: 0, x: 1.1, y: 0.05,
                rotate: 0
            },
            end: {
                percent: 1, x: 0.069, y: 0.9375,
                rotate: 0
            }
        },
        {
            name: 'image_01_12',
            start: {
                percent: 0, x: -0.9, y: -0.05,
                rotate: 0
            },
            end: {
                percent: 1, x: 0.150, y: 0.9375,
                rotate: 0
            }
        },
        {
            name: 'image_01_22',
            start: {
                percent: 0, x: -1.9, y: 0.05,
                rotate: 0
            },
            end: {
                percent: 1, x: 0.232, y: 0.9375,
                rotate: 0
            }
        },


        // image_02
        {
            name: 'image_02_00',
            start: {
                percent: 0, x: 1.1, y: 0.05,
                rotate: -15
            },
            end: {
                percent: 1, x: -0.993, y: 0.9144,
                rotate: -15
            }
        },
        {
            name: 'image_02_10',
            start: {
                percent: 0, x: -0.9, y: 0,
                rotate: -15
            },
            end: {
                percent: 1, x: -0.9113, y: 0.9144,
                rotate: -15
            }
        },
        {
            name: 'image_02_20',
            start: {
                percent: 0, x: 0, y: -0.05,
                rotate: -15
            },
            end: {
                percent: 1, x: -0.8313, y: 0.9144,
                rotate: -15
            }
        },
        {
            name: 'image_02_01',
            start: {
                percent: 0, x: 1.1, y: 0,
                rotate: -15
            },
            end: {
                percent: 1, x: -0.993, y: 0.93,
                rotate: -15
            }
        },
        {
            name: 'image_02_11',
            start: {
                percent: 0, x: 0, y: 0.11,
                rotate: -15
            },
            end: {
                percent: 1, x: -0.9113, y: 0.93,
                rotate: -15
            }
        },
        {
            name: 'image_02_21',
            start: {
                percent: 0, x: 1.1, y: 0,
                rotate: -15
            },
            end: {
                percent: 1, x: -0.8313, y: 0.93,
                rotate: -15
            }
        },
        {
            name: 'image_02_02',
            start: {
                percent: 0, x: 1.1, y: 0.05,
                rotate: -15
            },
            end: {
                percent: 1, x: -0.993, y: 0.9456,
                rotate: -15
            }
        },
        {
            name: 'image_02_12',
            start: {
                percent: 0, x: -0.9, y: -0.05,
                rotate: -15
            },
            end: {
                percent: 1, x: -0.9113, y: 0.9456,
                rotate: -15
            }
        },
        {
            name: 'image_02_22',
            start: {
                percent: 0, x: -1.9, y: 0.05,
                rotate: -15
            },
            end: {
                percent: 1, x: -0.8313, y: 0.9456,
                rotate: -15
            }
        },

        // image_03
        {
            name: 'image_03_00',
            start: {
                percent: 0, x: 1.1, y: 0.05,
                rotate: 5
            },
            end: {
                percent: 1, x: 1.087, y: 0.8901,
                rotate: 5
            }
        },
        {
            name: 'image_03_10',
            start: {
                percent: 0, x: -0.9, y: 0,
                rotate: 5
            },
            end: {
                percent: 1, x: 1.168, y: 0.8901,
                rotate: 5
            }
        },
        {
            name: 'image_03_20',
            start: {
                percent: 0, x: 0, y: -0.05,
                rotate: 5
            },
            end: {
                percent: 1, x: 1.249, y: 0.8901,
                rotate: 5
            }
        },
        {
            name: 'image_03_01',
            start: {
                percent: 0, x: 1.1, y: 0,
                rotate: 5
            },
            end: {
                percent: 1, x: 1.087, y: 0.9058,
                rotate: 5
            }
        },
        {
            name: 'image_03_11',
            start: {
                percent: 0, x: 0, y: 0.11,
                rotate: 5
            },
            end: {
                percent: 1, x: 1.168, y: 0.9058,
                rotate: 5
            }
        },
        {
            name: 'image_03_21',
            start: {
                percent: 0, x: 1.1, y: 0,
                rotate: 5
            },
            end: {
                percent: 1, x: 1.249, y: 0.9058,
                rotate: 5
            }
        },
        {
            name: 'image_03_02',
            start: {
                percent: 0, x: 1.1, y: 0.05,
                rotate: 5
            },
            end: {
                percent: 1, x: 1.087, y: 0.9214,
                rotate: 5
            }
        },
        {
            name: 'image_03_12',
            start: {
                percent: 0, x: -0.9, y: -0.05,
                rotate: 5
            },
            end: {
                percent: 1, x: 1.168, y: 0.9214,
                rotate: 5
            }
        },
        {
            name: 'image_03_22',
            start: {
                percent: 0, x: -1.9, y: 0.05,
                rotate: 5
            },
            end: {
                percent: 1, x: 1.249, y: 0.9214,
                rotate: 5
            }
        }
    ];

    resize();
    initMovingElements();

    function initMovingElements() {
        for (let i = 0; i < _positions.length; i++) {
            _positions[i].diff = {
                percent: _positions[i].end.percent - _positions[i].start.percent,
                x: _positions[i].end.x - _positions[i].start.x,
                y: _positions[i].end.y - _positions[i].start.y,
            };
            let el = document.getElementsByClassName('jigsaw_piece ' + _positions[i].name)[0];
            _movingElements.push(el);
        }
    }

    function resize() {
        _width = window.innerWidth;
        _height = window.innerHeight;
        _scrollHeight = _containerHeight - _height;
    }

    function updateElements() {

        for (let i = 0; i < _movingElements.length; i++) {
            let p = _positions[i];
            if (_scrollPercent <= p.start.percent) {
                _movingElements[i].style[_jsPrefix + 'Transform'] = '' +
                    'rotate( ' + p.start.rotate + 'deg)' +
                    'translate3d(' + (p.start.x * _width) + 'px, ' + (p.start.y * _containerHeight) + 'px, 0px)';
            } else if (_scrollPercent >= p.end.percent) {
                _movingElements[i].style[_jsPrefix + 'Transform'] = '' +
                    'rotate( ' + p.end.rotate + 'deg)' +
                    'translate3d(' + (p.end.x * _width) + 'px, ' + (p.end.y * _containerHeight) + 'px, 0px)';
            } else {
                _movingElements[i].style[_jsPrefix + 'Transform'] = '' +
                    'rotate( ' + p.start.rotate + 'deg)' +
                    'translate3d(' + (p.start.x * _width + (p.diff.x * (_scrollPercent - p.start.percent) / p.diff.percent * _width)) + 'px, ' +
                    (p.start.y * _containerHeight + (p.diff.y * (_scrollPercent - p.start.percent) / p.diff.percent * _containerHeight)) + 'px, 0px)';
            }
        }
    }

    function loop() {
        _scrollOffset = window.pageYOffset || window.scrollTop;
        _scrollPercent = _scrollOffset / _scrollHeight || 0;
        updateElements();

        requestAnimationFrame(loop);
    }

    loop();

    window.addEventListener('resize', resize);

    function prefix() {
        let styles = window.getComputedStyle(document.documentElement, ''),
            pre = (Array.prototype.slice
                    .call(styles)
                    .join('')
                    .match(/-(moz|webkit|ms)-/) || (styles.OLink === '' && ['', 'o'])
            )[1],
            dom = ('WebKit|Moz|MS|O').match(new RegExp('(' + pre + ')', 'i'))[1];
        return {
            dom: dom,
            lowercase: pre,
            css: '-' + pre + '-',
            js: pre[0].toUpperCase() + pre.substr(1)
        };
    }
});